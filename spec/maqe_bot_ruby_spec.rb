require 'maqe_bot'

RSpec.describe MaqeBot do

  context "get input" do
    it "should accept only uppercase" do
      app = MaqeBot.new
      input = "Rlw32Lr".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should accept only positive integers" do
      app = MaqeBot.new
      input = "RLW-32LR".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should accept only L, R or WX" do
      app = MaqeBot.new
      input = "ERLW20AR".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should not accept number after L" do
      app = MaqeBot.new
      input = "L10RW34".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should not accept number after R" do
      app = MaqeBot.new
      input = "LR4W34".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should be a number after W" do
      app = MaqeBot.new
      input = "LRWL".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(false)
    end

    it "should accept uppercase, L, R or WX" do
      app = MaqeBot.new
      input = "LRW34LRW20L".split("")

      # Check that hash contains orders
      expect(app.valid_input?(input)).to eq(true)
    end
  end 

  context "walk robot" do
    it "should turn left" do
      app = MaqeBot.new
      input = "L".split("")
      input = app.parse_input(input)

      map = app.walk_robot(input)
      
      expect(map[:position]).to eq([0,0])
      expect(map[:direction]).to eq("W")
    end

    it "should turn right" do
      app = MaqeBot.new
      input = "R".split("")
      input = app.parse_input(input)

      map = app.walk_robot(input)
      
      expect(map[:position]).to eq([0,0])
      expect(map[:direction]).to eq("E")
    end

    it "should walk straight X steps" do
      app = MaqeBot.new
      input = "W20".split("")
      input = app.parse_input(input)

      map = app.walk_robot(input)
      
      expect(map[:position]).to eq([0,20])
      expect(map[:direction]).to eq("N")
    end
    
    it "should return the final robot position and direction" do
      app = MaqeBot.new
      input = "RW15RW1".split("")
      input = app.parse_input(input)

      map = app.walk_robot(input)
      
      expect(map[:position]).to eq([15,-1])
      expect(map[:direction]).to eq("S")
    end
  end 
end
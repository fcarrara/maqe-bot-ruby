class MaqeBot

  # Gets the input (commands) from the command line
  def get_command
    puts "Please enter command: "
    gets.chomp
  end
  
  # Validates that the input is correct.
  # It rejects any lowercase, symbol or negative numbers
  # Returns true or false
  def valid_input?(commands)
    commands.each_with_index do |command, index|
      case command
      when "L", "R"
        next_command = commands[index + 1]
        if !is_last_command?(commands, index) && is_number?(next_command)
          return false
        end
      when "W"
        next_command = commands[index + 1]
        if is_last_command?(commands, index)
          return false
        elsif !is_number?(next_command)
          return false
        end
      when /[0-9]/
        if index == 0
          return false
        end
      else 
        return false
      end
    end

    return true
  end

  # Check if a single entry command is a number
  # Returns true or false
  def is_number?(command)
    command !~ /\D/
  end

  # Check if the command is the last one in the list
  # Returns true or false
  def is_last_command?(commands, index)
    commands[index + 1].nil?
  end

  # Parse the string input to a list
  # Returns a list containing the commands. eg. ["L", "R", "W10"]
  def parse_input(commands)
    new_commands = []
    walk = ""

    commands.each_with_index do |command, i|
      if (command == "W")
        if walk != ""
          new_commands.push(walk)
        end
        walk = "W"
      elsif is_number?(command)
        walk = walk ++ command
        if i == (commands.size - 1)
          new_commands.push(walk)
        end
      elsif walk != ""
        new_commands.push(walk)
        new_commands.push(command)
        walk = ""
      else
        new_commands.push(command)
      end
    end

    new_commands
  end

  # Rotates the robot direction to the left
  # Returns updated direction
  def turn_left(direction)
    case direction
    when "N"
      return "W"
    when "W"
      return "S"
    when "S"
      return "E"
    else # E
      return "N"
    end
  end

  # Rotates the robot direction to the right
  # Returns updated direction
  def turn_right(direction)
    case direction
    when "N"
      return "E"
    when "E"
      return "S"
    when "S"
      return "W"
    else # W
      return "N"
    end
  end

  # Move the robot forward the number of steps specified
  # Returns the new position [X,Y]
  def move_forward(direction, position, steps)
    case direction
    when "N"
      position[1] = position[1] + steps
    when "E"
      position[0] = position[0] + steps
    when "S"
      position[1] = position[1] - steps
    else # W
      position[0] = position[0] - steps
    end
    position
  end

  # Parse the single char direction to a full string.
  # Returns the full string direction
  def direction_to_string(direction)
    case direction
    when "N"
      "North"
    when "E"
      "East"
    when "S"
      "South"
    else
      "West"
    end
  end

  # Read the commands one by one and moves the robot accordingly.
  # Returns a map containing the final position and direction
  def walk_robot(commands)
    position = [0,0]
    direction = "N"

    commands.each do |command|
      case command
      when "L"
        direction = turn_left(direction)
      when "R"
        direction = turn_right(direction)
      else # W + steps
        steps = command[1..-1].to_i
        position = move_forward(direction, position, steps)
      end
    end

    { position: position, direction: direction }

  end

  # Start the command line program
  # Returns a string containing the position and direction (full string) 
  def run
    input = get_command.split("")
    
    while !valid_input?(input)
      puts "The command entered is not valid. Please try again: "
      input = gets.chomp.split("")
    end

    commands = parse_input(input)
    map = walk_robot(commands)
    "X: " + map[:position][0].to_s + " Y: " + map[:position][1].to_s + " Direction: " + direction_to_string(map[:direction])
  end

end

# Run from command line
app = MaqeBot.new
puts app.run
